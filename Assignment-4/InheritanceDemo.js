var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Company = (function () {
    function Company(name, city) {
        this.name = name;
        this.city = city;
    }
    Company.prototype.address = function (streetName) {
        return ("Company Name: " + this.name + "Street Name: " + streetName + "City: " + this.city);
    };
    return Company;
}());
var Employee = (function (_super) {
    __extends(Employee, _super);
    function Employee(first, last, year, name, city) {
        var _this = _super.call(this, name, city) || this;
        _this.firstName = first;
        _this.lastName = last;
        _this.birthYear = year;
        return _this;
    }
    Employee.prototype.printEmployeeDetails = function () {
        console.log("Employee Name: " + this.firstName + " " + this.lastName);
    };
    Employee.prototype.age = function () {
        var employeeAge = 2017 - this.birthYear;
        console.log("Age: " + employeeAge);
        return employeeAge;
    };
    Employee.prototype.printCollegeDetails = function () {
        var details = _super.prototype.address.call(this, "Vaishali Nagar");
        console.log(details);
    };
    return Employee;
}(Company));
var user = new Employee("Ritika", "Innani", 1994, "InTimeTec", "Jaipur");
user.printEmployeeDetails();
user.age();
user.printCollegeDetails();
