var GenericDemo = (function () {
    function GenericDemo() {
    }
    GenericDemo.prototype.print = function (a, b) {
        console.log("values are: " + a + "and" + b);
    };
    return GenericDemo;
}());
var obj1 = new GenericDemo();
obj1.print(2, 4);
var obj2 = new GenericDemo();
obj2.print("Hello", "World");
