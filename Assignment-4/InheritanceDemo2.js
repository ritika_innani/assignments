var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Shape = (function () {
    function Shape(color) {
        this.color = color;
    }
    return Shape;
}());
var Polygon = (function (_super) {
    __extends(Polygon, _super);
    function Polygon(color, sides) {
        var _this = _super.call(this, color) || this;
        _this.sides = sides;
        return _this;
    }
    return Polygon;
}(Shape));
var Hexagon = (function (_super) {
    __extends(Hexagon, _super);
    function Hexagon(color, sides, interiorAngle) {
        var _this = _super.call(this, color, sides) || this;
        _this.interiorAngle = interiorAngle;
        return _this;
    }
    Hexagon.prototype.displayProperties = function () {
        console.log("Shape: polygon, color: " + this.color + ", number of sides: " + this.sides + ", Interior Angle: " + this.interiorAngle);
    };
    return Hexagon;
}(Polygon));
var obj = new Hexagon("blue", 6, 120);
obj.displayProperties();
