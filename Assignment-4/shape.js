var Rectangle = (function () {
    function Rectangle(length, breadth) {
        this.length = length;
        this.breadth = breadth;
    }
    Rectangle.prototype.calculateArea = function () {
        console.log("Area of rectangle = " + this.length * this.breadth);
        return this.length * this.breadth;
    };
    return Rectangle;
}());
var Circle = (function () {
    function Circle(radius) {
        this.radius = radius;
    }
    Circle.prototype.calculateArea = function () {
        console.log("Area of circle = " + 3.14 * this.radius * this.radius);
        return 3.14 * this.radius * this.radius;
    };
    return Circle;
}());
var obj = new Rectangle(5, 6);
obj.calculateArea();
var obj1 = new Circle(7);
obj1.calculateArea();
