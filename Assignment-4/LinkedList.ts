class NewNode<T>{      // New Node
    value: T;
    next: NewNode<T>;
    constructor(value: T){
        this.value = value;
        this.next = null;
    }
}

class LinkedList<T>{               
    head: NewNode<T>;

    addNode(value: T): boolean{           // insert new node at the end of the list
        if(this.head == null){
            this.head = new NewNode<T>(value);
            return true;
        }else{
            let currentNode = this.head;
            while(currentNode.next != null){
                currentNode = currentNode.next;
            }
            currentNode.next = new NewNode<T>(value);
            return true;
        }   
    }
    
    addAtStart(value: T): boolean{
        if(this.head == null){
            this.head = new NewNode<T>(value);
            return true;
        } else{
            let currentNode = new NewNode<T>(value);
            currentNode.next = this.head;
            this.head = currentNode;
            return true;
        }
    }
    
    deleteNode(): boolean{        //deletes first item of the list
        if (!this.head){
            console.log('Nothing to delete');
            return true;
        }
        this.head = this.head.next;
        return true;
    }
    
    deleteByValue(value): boolean{
        var currentNode = this.head;

        if(!currentNode){
            console.log('Nothing to delete');
            return true;
        }
        if(currentNode.value == value){    //for first element
            this.head = currentNode.next;
            return true;
        } else{
            var previous = null;
            while(true){
                if(currentNode.value == value){
                    if (currentNode.next) { 
                        previous.next = currentNode.next;
                    } else {
                        previous.next = null;
                    }
                    currentNode = null;
                    break;
                } else{
                    previous = currentNode;
                    currentNode = currentNode.next;
                }
            }
        }
    }
    
    displayLinkedList(): void{            //  display the linked list    
        let currentNode = this.head;
        if(this.head == null){
            console.log('empty linked list');
        } else{
            while(currentNode.next != null){
                console.log(currentNode.value + " " );
                document.getElementById('myDiv').innerHTML += currentNode.value + " ";
                currentNode = currentNode.next; 
            }
            console.log(currentNode.value + " " );
        }
        
    }
}

//    console.log()

let newList = new LinkedList<number>();
newList.addNode(56);
newList.addNode(63);
newList.addNode(45);
newList.deleteByValue(45);
newList.addNode(28);
newList.deleteNode();
newList.addAtStart(100);
newList.displayLinkedList();

//let newList = new LinkedList<string>();
//newList.addNode("hello");
//newList.addNode("ritika");
//newList.addNode("how");
//newList.addNode("are");
//newList.deleteNode();
//newList.displayLinkedList();
