var global_num:number = 12      //global variable
class Demo{
    num_val = 13;             //class variable 
    static sval = 10;         //static variable
    fullName: string;
    constructor(public firstName, public lastName){
        this.fullName = firstName + " " + lastName;
    } 
    greet() : void{
         var local_num = 14;        //local variable
        console.log('Hello World!');
    }
}

interface Person{
    firstName: string;
    lastName: string;
}

function greeter(person : Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

var user = new Demo("ritika", "innani");
document.body.innerHTML = greeter(user);
user.greet();
console.log("Global num: "+global_num)  
console.log(Demo.sval)   //static variable  
console.log("Class num: "+user.num_val)