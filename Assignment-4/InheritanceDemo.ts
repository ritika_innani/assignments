interface EmployeeAge{
    birthYear: number;
    age: () => number;
}

class Company{
    constructor(public name: string, public city: string){
    }
    public address(streetName: string){
        return("Company Name:" + this.name + " Street Name:" + streetName + " City:" + this.city);
    }
}

class Employee extends Company implements EmployeeAge{
    firstName: string;
    lastName: string;
    birthYear: number;
    constructor(first: string, last: string, year: number, name: string, city: string ){
        super(name, city);
        this.firstName = first;
        this.lastName = last;
        this.birthYear = year;
    }
    
    printEmployeeDetails(){
        console.log("Employee Name: " + this.firstName + " " + this.lastName);
    }

    age(){
        var employeeAge = 2017 - this.birthYear;
        console.log("Age: " + employeeAge);
        return employeeAge
    }

    printCompanyDetails(){
        var details = super.address("Vaishali Nagar");
        console.log(details);
    }

}

    var user = new Employee("Ritika", "Innani", 1994, "InTimeTec", "Jaipur");
    user.printEmployeeDetails();
    user.age();
    user.printCompanyDetails();