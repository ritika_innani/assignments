class Shape{

    color: string;
    constructor(color: string){
        this.color = color;
    }
}

class Polygon extends Shape{

    sides: number;
    constructor(color: string, sides: number){
        super(color);
        this.sides = sides;
    }
}

class Hexagon extends Polygon{
    interiorAngle: number;
    constructor(color: string, sides: number, interiorAngle: number){
        super(color, sides);
        this.interiorAngle = interiorAngle;
    }
    
    displayProperties(){
        console.log("Shape: polygon, color: " + this.color + ", number of sides: " + this.sides + ", Interior Angle: " + this.interiorAngle );
    }
}

var obj = new Hexagon("blue", 6, 120); 
    obj.displayProperties();