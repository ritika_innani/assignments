 
interface Shape{
    calculateArea: ()=>number
}

class Rectangle implements Shape{
    
    constructor(public length: number, public breadth: number) {   }
    calculateArea() {
        console.log("Area of rectangle = " + this.length*this.breadth);
        return this.length*this.breadth;
    }
}

class Circle implements Shape{
    
    constructor(public radius: number) {   }
    calculateArea() {
        console.log("Area of circle = " + 3.14 * this.radius * this.radius);
        return 3.14 * this.radius * this.radius;
    }
}

var obj = new Rectangle(5,6);
obj.calculateArea();

var obj1 = new Circle(7); 
obj1.calculateArea();