var NewNode = (function () {
    function NewNode(value) {
        this.value = value;
        this.next = null;
    }
    return NewNode;
}());
var LinkedList = (function () {
    function LinkedList() {
    }
    LinkedList.prototype.addNode = function (value) {
        if (this.head == null) {
            this.head = new NewNode(value);
            return true;
        }
        else {
            var currentNode = this.head;
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            currentNode.next = new NewNode(value);
            return true;
        }
    };
    LinkedList.prototype.addAtStart = function (value) {
        if (this.head == null) {
            this.head = new NewNode(value);
            return true;
        }
        else {
            var currentNode = new NewNode(value);
            currentNode.next = this.head;
            this.head = currentNode;
            return true;
        }
    };
    LinkedList.prototype.deleteNode = function () {
        if (!this.head) {
            console.log('Nothing to delete');
            return true;
        }
        this.head = this.head.next;
        return true;
    };
    LinkedList.prototype.deleteByValue = function (value) {
        var currentNode = this.head;
        if (!currentNode) {
            console.log('Nothing to delete');
            return true;
        }
        if (currentNode.value == value) {
            this.head = currentNode.next;
            return true;
        }
        else {
            var previous = null;
            while (true) {
                if (currentNode.value == value) {
                    if (currentNode.next) {
                        previous.next = currentNode.next;
                    }
                    else {
                        previous.next = null;
                    }
                    currentNode = null;
                    break;
                }
                else {
                    previous = currentNode;
                    currentNode = currentNode.next;
                }
            }
        }
    };
    LinkedList.prototype.displayLinkedList = function () {
        var currentNode = this.head;
        if (this.head == null) {
            console.log('empty linked list');
        }
        else {
            while (currentNode.next != null) {
                console.log(currentNode.value + " ");
                document.getElementById('myDiv').innerHTML += currentNode.value + " ";
                currentNode = currentNode.next;
            }
            console.log(currentNode.value + " ");
        }
    };
    return LinkedList;
}());
//    console.log()
var newList = new LinkedList();
newList.addNode(56);
newList.addNode(63);
newList.addNode(45);
newList.deleteByValue(45);
newList.addNode(28);
newList.deleteNode();
newList.addAtStart(100);
newList.displayLinkedList();
//let newList = new LinkedList<string>();
//newList.addNode("hello");
//newList.addNode("ritika");
//newList.addNode("how");
//newList.addNode("are");
//newList.deleteNode();
//newList.displayLinkedList();
