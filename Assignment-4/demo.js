var global_num = 12; //global variable
var Demo = (function () {
    function Demo(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.num_val = 13; //class variable 
        this.fullName = firstName + " " + lastName;
    }
    Demo.prototype.greet = function () {
        var local_num = 14; //local variable
        console.log('Hello World!');
    };
    Demo.sval = 10; //static variable
    return Demo;
}());
function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var user = new Demo("ritika", "innani");
document.body.innerHTML = greeter(user);
user.greet();
console.log("Global num: " + global_num);
console.log(Demo.sval); //static variable  
console.log("Class num: " + user.num_val);
