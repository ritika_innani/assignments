import {Directive, ElementRef, Renderer} from 'angular2/core'
@Directive({
    selector: '[autoGrow]',
    host: {
        '(focus)': 'onFocus()',
        '(blur)': 'onBLur()'
    }
 })

export class AutoGrowDirective{
    constructor(private elem: ElementRef, private renderer: Renderer){
        
    }
    onFocus(){
        this.renderer.setElementStyle(this.elem, 'width', '250');
    }
    onBlur(){
        this.renderer.setElementStyle(this.elem, 'width', '100');
    }
}