import {Component} from 'angular2/core'
import {LoginService} from './login.service'
import {AutoGrowDirective} from './auto-grow.directive'
    
@Component({
    selector: 'login',
    template: `
        <h2>LOGIN PAGE</h2>
        {{ title }}
        <br />
        <label>Email:</label>
        <input type="text" autoGrow />
        <ul>
        <li *ngFor="#user of users">{{user}}</li>
        </ul>
        `,
    providers: [LoginService],
    directives: [AutoGrowDirective]
})
export class LoginComponent{
    title: string = "Login Page";
    users;
    constructor(loginService: LoginService){
    this.users = loginService.getUsers();
    }
}