import { Component } from '@angular/core';
import { LocalStorageService } from './localstorage.service';

@Component({ 
    selector: 'app-root',
    templateUrl: './app.component.html',
    providers: [LocalStorageService],  
})

export class AppComponent {
    
}
