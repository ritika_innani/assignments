import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '../localstorage.service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class DashboardComponent implements OnInit{
    public allRepositories = [];
    public userName = sessionStorage.getItem('uname');
    constructor(private localService: LocalStorageService, private route: Router){}
    
    ngOnInit(){
        if(this.localService.getRepositories())
        this.allRepositories = this.localService.getRepositories();
    }
    
    onSubmit(value: any){
        console.log(value);
        this.allRepositories.push(value);
        this.localService.setRepositories(this.allRepositories);
    }
    
    onDelete(value:any){
        console.log(value)  ; 
        this.localService.deleteRepository(value) ; 
        this.allRepositories = this.localService.getRepositories();
    }
}