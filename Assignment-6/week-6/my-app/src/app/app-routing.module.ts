import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './pagenotfound.component';

const routes: Routes=[
            {path: '', redirectTo: '/home', pathMatch: 'full'},
            {path: 'home' , component: HomeComponent, children:[
                {path: '', component: LoginComponent, outlet: 'child1' },
                {path: 'login', component: LoginComponent, outlet: 'child1'},
                {path: 'signup', component: SignupComponent, outlet: 'child1'} 
            ]},
            {path: 'dashboard', component: DashboardComponent},
            {path: '**', component: PageNotFoundComponent }
    ];

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]
})

export class AppRoutingModule{}
export const routingComponents = [LoginComponent, SignupComponent, DashboardComponent, PageNotFoundComponent]
