import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '../localstorage.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styles: [`.displayDiv{display:block}
            .hideDiv{display:none}
            .Warning{color:red}`]
  
})
export class SignupComponent implements OnInit {
 
  public allUsers = [];
    
    constructor(private localService: LocalStorageService, private route: Router){}
    ngOnInit(){
    if(this.localService.getUsers())
        this.allUsers = this.localService.getUsers();
    }
    
    onSubmit(value: any){
        console.log(value);
        this.allUsers.push(value);
        this.localService.setUsers(this.allUsers);
        sessionStorage.setItem('uname', value.uname);
        this.route.navigate(['/dashboard']);
    }
}
