import { Injectable } from '@angular/core';

@Injectable()

export class LocalStorageService{
    public allUsers = [];
public allRepositories=[];
    getUsers(){
        if(JSON.parse(localStorage.getItem("users")) != null){
            this.allUsers = JSON.parse(localStorage.getItem("users"));
            return this.allUsers;
        }
    }
    setUsers(userList){
        localStorage.setItem('users', JSON.stringify(userList));
    }
    
    getRepositories(){
        if(JSON.parse(localStorage.getItem("repositories")) != null){
            this.allRepositories = JSON.parse(localStorage.getItem("repositories"));
            return this.allRepositories;
        }
    }
    setRepositories(repoList){
         localStorage.setItem('repositories', JSON.stringify(repoList));
    }
    
    deleteRepository(repository:any){
        this.allRepositories = JSON.parse(localStorage.getItem("repositories"));
        let index = this.allRepositories.indexOf(repository);  
        this.allRepositories.splice(index-1,1); 
        localStorage.setItem('repositories', JSON.stringify(this.allRepositories));
    }
}