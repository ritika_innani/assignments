module.exports = {
    dbConfig: {
        url: 'mongodb://localhost:27017/task_management',
        database: 'task_management'
    },
    
    port: 8000,
    
    cors: {
        origin: 'http://localhost:4200'
    }
}

