var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
var db = require('../util/dbconnect');

let emailChecker = function(email){
    if(!email){
        return false;
    } else{
        if(email.length < 5 || email.length > 30){
            return false;
        } else{
            const regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            return regExp.test(email);
        } 
    }
};

var emailValidators = [{
    validator: emailChecker,
    message: 'Please provide valid Email'
}];

var userSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true, validate: emailValidators},
    password: {type: String, required: true},
    company: {type: String, required: true}
});

userSchema.methods.comparePassword = function(password) {
  return (password==this.password);
};

module.exports = db.model('User', userSchema);