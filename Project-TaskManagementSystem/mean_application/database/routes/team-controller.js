var Team = require('../mongodb/models/team');
var jwt = require('jsonwebtoken');

module.exports = function(router){

    router.post('/team', function(req, res){
       if(!req.body.memberId){
           res.json({success: false, message: "no user added"});
       } else{
           let team = new Team({
               email: req.body.userEmail,
               member: req.body.memberId
           });
           console.log(team);
           team.save(function(err){
               if(err){
                   console.error(err);
                   res.json({success: false, message: 'could not add member', err})
               }
               else{
                   res.json({success: true, message: 'Member Added'});
               }
           });
       }
    });

    router.get('/team/:email', function(req,res) {
        if (req.params.email) {
            Team.find({email: req.params.email}, function (err, team) {
                if (err) {
                    res.json({success: false, message: err});
                } else {
                    if (!team) {
                        res.json({success: false, message: 'No team added yet'});
                    } else {
                        res.json({success: true, teams: team});
                    }
                }
            });
        }
    });



    return router;
}