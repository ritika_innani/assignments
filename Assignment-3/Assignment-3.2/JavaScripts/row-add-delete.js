var uname = sessionStorage.getItem('uname');

//function for adding a row to table dynamically
function addRepository(){
    var allRepositories = [];
    
    if(document.newRepoForm.repository.value == "" || document.newRepoForm.project.value == "" || document.newRepoForm.owner.value == ""){
        alert('kindly fill all the entries!');
        return false;
    }
    
    var repository = {
        name: document.newRepoForm.repository.value,
        project: document.newRepoForm.project.value,
        owner: document.newRepoForm.owner.value
    };
    
    if(JSON.parse(localStorage.getItem(uname)) != null){
        allRepositories = JSON.parse(localStorage.getItem(uname));
    }
    
    allRepositories.push(repository);
    localStorage.setItem(uname, JSON.stringify(allRepositories));
    
    var tbody = document.getElementById('tbody');
    var row = tbody.insertRow();
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    
    cell1.innerHTML = '<span class="repositoryField">'+repository.name+'</span>';
    cell2.innerHTML = '<span class="ProjectField">'+repository.project+'</span>';
    cell3.innerHTML = '<span class="OwnerField">'+repository.owner+'</span>';
    cell4.innerHTML = '<div class="deleteButton"><button class="button1 outline delete" id="button'+deleteButtonId+'" onclick="deleteRepository(this.id);" title="delete"><span class="fa fa-trash-o"></span></button></div>';
    deleteButtonId++; 
    
    document.newRepoForm.repository.value = null;
    document.newRepoForm.project.value = null;
    document.newRepoForm.owner.value = null;
    
}

//function for deleting a row dynamically from table

function deleteRepository(buttonId){
    var rowIndex = document.getElementById(buttonId).parentNode.parentNode.parentNode.rowIndex;
    var repoIndex = rowIndex - 2;
    document.getElementById('tbody').deleteRow(rowIndex-1);
    
    allRepositories = JSON.parse(localStorage.getItem(uname));
    allRepositories.splice(repoIndex,1);
    localStorage.setItem(uname, JSON.stringify(allRepositories));
    
}


