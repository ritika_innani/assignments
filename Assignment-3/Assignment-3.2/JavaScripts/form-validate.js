var allUsers = [];
  // function for validating a new user
function validateForm() {
    document.getElementById('nameWarning').innerHTML = "";
    document.getElementById('emailWarning').innerHTML = "";
    document.getElementById('passWarning').innerHTML = "";
    
    if(document.myForm.firstname.value == "" || document.myForm.emailid.value == "" || document.myForm.uname.value == "" || document.myForm.pass.value == "" ){
       alert('please fill all the required fields');
        return false;
       }

    var pname = document.myForm.firstname.value + " " + document.myForm.lastname.value;
    var person = {
    name      : pname,
    email     : document.myForm.emailid.value,
    uname     : document.myForm.uname.value,
    pass      : document.myForm.pass.value
    };
    
    //for calling fields validation
    if(validateName(document.myForm.firstname.value)){
      if(validateName(document.myForm.lastname.value)){
       if(validateEmail(person.email)){         
         if(validatePass(person.pass)){
    } else return false;
    } else {document.getElementById('emailWarning').innerHTML = "Please provide a proper Email Address"; return false;}
    } else return false;
    } else return false;
    
    if(JSON.parse(localStorage.getItem("users")) != null){
        allUsers = JSON.parse(localStorage.getItem("users"));
        for(var index=0; index<allUsers.length; index++){
        if(person.uname == allUsers[index].uname){
           document.getElementById('passWarning').innerHTML="Username not available"; return false; 
        }
    } 
    }
    allUsers.push(person);
    // storing values in local & session storage
    localStorage.setItem('users', JSON.stringify(allUsers)); 
    sessionStorage.setItem('uname', person.uname);
    
    document.getElementById('fixedForm').style.display = 'none';
    document.getElementById('slidingForm').style.display = 'block';
    document.getElementById('loginBanner').innerHTML = "You are Succesfully Registered !  Please Log In";
    document.getElementById('loginForm').uname.value = sessionStorage.getItem('uname');
    return true;
 }  

    // function for name validation
    function validateName(fullname){
        var letters = /^[A-Za-z]+$/;
        if(!(fullname.match(letters))){ 
            document.getElementById('nameWarning').innerHTML="Please provide a proper name";
           return false;
        }
        return true;
    }

    // function for email validation
    function validateEmail(email){
         var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return reg.test(email);
    }

    // function for password validation
    function validatePass(pass){
        var regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if(pass.length<8){  
            document.getElementById('passWarning').innerHTML="Password should be atleast 8 characters long";  
        return false; 
        } else if(!regularExpression.test(pass)) {
            document.getElementById('passWarning').innerHTML="Password should contain atleast one number and one special character"; 
            return false;
        }
        return true;
    }
 
//function for validating existing user
function validateUser(){
    var uname = document.loginForm.uname.value;
    var pass = document.loginForm.pass.value;
    var storedUsers = JSON.parse(localStorage.getItem("users"));
    
    if(JSON.parse(localStorage.getItem("users"))){
        for(var index=0; index<storedUsers.length; index++){
            if(uname == storedUsers[index].uname){
                if(pass == storedUsers[index].pass){
                    sessionStorage.setItem('uname',uname);
                    window.location.href = 'dashboard.html';
                    return true;
                }else{
                    document.getElementById('unameWarning').innerHTML="Username or Password is incorrect";
                    return false;
                }
            }
        }
    }
    document.getElementById('unameWarning').innerHTML="Username does not exist";
    return false;
}