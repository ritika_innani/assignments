var deleteButtonId = 1;
window.onload = function() {
 var uname = sessionStorage.getItem('uname');
    document.title = 'Welcome ' + uname;
    document.getElementById('userName').innerHTML = uname;
    document.getElementById('accInfo').innerHTML = uname;
    
    if(JSON.parse(localStorage.getItem(uname))){
        allRepositories = JSON.parse(localStorage.getItem(uname));
        for(var index=0; index<allRepositories.length; index++){
            var tbody = document.getElementById('tbody');
            var row = tbody.insertRow();
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);

            cell1.innerHTML = '<span class="repositoryField">'+allRepositories[index].name+'</span>';
            cell2.innerHTML = '<span class="ProjectField">'+allRepositories[index].project+'</span>';
            cell3.innerHTML = '<span class="OwnerField">'+allRepositories[index].owner+'</span>';
            cell4.innerHTML = '<div class="deleteButton"><button class="button1 outline delete" id="button'+deleteButtonId+'" onclick="deleteRepository(this.id);" title="delete"><span class="fa fa-trash-o"></span></button></div>';
            deleteButtonId++;
        } 
    }
}

//function for displaying signout popup 

function displayLogout(){
    if(document.getElementById('logout').style.display === 'none'){
        document.getElementById('logout').style.display = 'block';
    } else{
        document.getElementById('logout').style.display = 'none'
    }
}

function logOutUser(){
    sessionStorage.removeItem('uname');
    window.location.href = 'signup.html';
}

function navToggle(buttonId){
     $button = '#' + buttonId;
    $('.navButtons').find('.navItem.active').removeClass('active');
			$($button).addClass('active');
}

/* function displayDeletePopup(){
    if(document.getElementById('deletePopup').style.display === 'none'){
        document.getElementById('deletePopup').style.display = 'block';
    } else{
        document.getElementById('deletePopup').style.display = 'none'
    }
} */