// importing modules
var express = require('express');
var app = express();
var router = express.Router();
var mongoose = require('mongoose');
var path = require('path');
var authentication = require('./router/authentication')(router);
var bodyParser = require('body-parser');
var cors = require('cors');

// port number
const port = 8000;

//connect to mongodb
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/customer_data', function(err){
    if(err){
        console.log('could not connect to database', err);
    } else{
        console.log('connected to database');
    }
});

// adding middleware
app.use(cors({
     origin: 'http://localhost:4200'
}));

//adding body-parser
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

// static files
app.use(express.static(__dirname + '/myApp/src/'));

//adding routes 
app.use('/authentication', authentication);

app.get('/', function(req,res){
    res.sendFile(path.join(__dirname + 'myApp/src/index.html'));
});

app.listen(port, function(){
    console.log('Listening on port 8000');
});