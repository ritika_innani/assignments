import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { AuthenticateService } from './services/authenticate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
    message: String;
    messageClass;
    allUsers;
    heroForm: FormGroup;
    
    constructor(private authService: AuthenticateService){
    }
    
    onSubmit(value: any, form: NgForm){
        console.log(value);
       this.authService.registerUser(value).subscribe(data => {
        console.log(data);
            if(!data.success){
                this.messageClass = 'alert alert-danger';
                this.message = data.message;
            } else{
                this.messageClass = 'alert alert-success';
                this.message = data.message;
                this.getAllUsers();
            }
        });
        this.heroForm.reset();
    }

    ngOnInit(){
    this.authService.getUserData().subscribe(data => {
        this.allUsers = data.data;
        console.log(this.allUsers);
    });
    }
    
    getAllUsers(){
        this.authService.getUserData().subscribe(data => {
            this.allUsers = data.data;
            console.log(this.allUsers);
            
        }); 
    }
}
