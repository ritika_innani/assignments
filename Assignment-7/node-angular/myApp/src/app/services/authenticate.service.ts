import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticateService {
    domain = "http://localhost:8000";

constructor(private http: Http) { }

    registerUser(user){
        return this.http.post(this.domain + '/authentication/register', user).map(res => res.json());
    }
    
    getUserData(){
        return this.http.get(this.domain + '/authentication/display').map(res => res.json());
    }
}


