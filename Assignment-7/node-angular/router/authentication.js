var User = require('../models/user');

module.exports = function(router){

    router.post('/register', function(req, res){
        if(!req.body.name.firstname){
            res.json({success:false, message: 'Must provide a first name'});
        } else{
            if(!req.body.email){
                res.json({success:false, message: 'Must provide an Email'});
            } else{
                if(!req.body.contact){
                    res.json({success:false, message: 'Must provide Contact number'});
                } else{            
                        let user = new User({
                            name: req.body.name,
                            email: req.body.email,
                            contact: req.body.contact,
                            address : req.body.address,
                            subscriptionType: req.body.subscriptionType  
                        });
                        console.log(user);
                        user.save(function(err){
                            if(err){
                                if(err.code == 11000){
                                    res.json({success: false, message:'email or contact no. already exists'});
                                } else{
                                    res.json({success: false, message: 'could not save user', err})
                                }
                            } else{
                                res.json({success: true, message: 'User Registered Successfully!'});
                            }
                        });
                    }
                }
            }
    });
    
    router.get('/display', function(req,res){
        User.find(function(err, data){
            if(err){
                res.json({success: false, data: 'no user data to display'});
            } else{
                console.log(data);
                res.json({success: true, data: data});
			
            }
        })
    });
    return router;
}