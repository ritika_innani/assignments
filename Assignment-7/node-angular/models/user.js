var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;

let emailChecker = function(email){
    if(!email){
        return false;
    } else{
        if(email.length < 5 || email.length > 30){
            return false;
        } else{
            const regExp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            return regExp.test(email);
        } 
    }
};

var emailValidators = [{
    validator: emailChecker,
    message: 'Please provide valid Email'
}];

var userSchema = new Schema({
    name: {
        firstname: {type: String, required: true},
        lastname: {type: String, required: true}
    },
    email: {type: String, required: true, unique: true, validate: emailValidators},
    contact: {type: String, required: true, unique: true},
    address: {
        street: {type: String, required: true},
        landmark: {type: String, required: true},
        city: {type: String, required: true},
        state: {type: String, required: true},
        postal: {type: String, required: true}
    },
    subscriptionType: {type: String, required: true}
});
//console.log(userSchema);

module.exports = mongoose.model('User', userSchema);