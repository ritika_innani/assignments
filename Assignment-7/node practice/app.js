

var mongo = require('mongodb').MongoClient;
var assert = require('assert');

var url = 'mongodb://localhost:27017/ritika';

mongo.connect(url, function(err, db) {
  assert.equal(null, err);
  var user = { username: "ritika@gmail.com", password: "ITT@123456" };
  db.collection("demo").insertOne(user, function(err, res) {
    assert.equal(null, err);
    console.log("1 document inserted");
    db.close();
  });
});

module.exports = {
  handleRequest: function(request, response) {
      response.writeHead(200, {'Content-Type': 'text/html'});
      response.send("hello world!");
  }
};