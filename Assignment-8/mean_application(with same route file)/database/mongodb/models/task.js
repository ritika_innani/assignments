var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
var db = require('../util/dbconnect');

var taskSchema = new Schema({
    project: {type: String, required: true},
    task: {type: String, required: true},
    description: {type: String, required: true},
   date_of_creation: {type: Date, default: Date.now()}
});

module.exports = db.model('Task', taskSchema);