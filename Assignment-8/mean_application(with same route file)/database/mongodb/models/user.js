var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var Schema = mongoose.Schema;
var db = require('../util/dbconnect');

var userSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    company: {type: String, required: true}
});

var User = db.model('User', userSchema);

var users = {
    addUser: function(Users, callback){
        let user = new User(Users);
        console.log(user);
        user.save(function(err){
            callback(err);
        });
    },
    
    getAllUsers: function(callback){
        User.find({}, function(err, result){
            console.log(result);
            callback(err, result);
        })
    },
    
    getUserByEmail: function(email, callback){
        User.findOne({email}, function(err, result){
            console.log(result);
            callback(err, result);
        });
    } 
    
//    deleteUser: function(id, callback){
//        
//    },
    
//    updateUser:function(id, User, callback){
//        return db.query("update users set name=?,email=?,password=?,company=? where user_id=?",[User.name,User.email,User.password,User.company,id],function(err, result){
//            console.log(result);
//            callback(err, result);
//        });
//    }
}

module.exports = users;