var db=require('../util/dbconnect');

var users = {
    addUser: function(User, callback){
        db.query("INSERT INTO users(name,email,password,company) values(?,?,?,?)", [User.name, User.email, User.password, User.company], function(err,result){
            callback(err, result);
        });
    },
    
    getAllUsers: function(callback){
        db.query("Select * from users", function(err,result){
            console.log(result);
            callback(err, result);   
        });
    },
    
    getUserByEmail: function(email, callback){
        db.query("Select * from users where email=?",[email], function(err, result){
            console.log(result);
            callback(err, result);
        });
    },
    
    deleteUser: function(id, callback){
        db.query("delete from users where email=?",[id],function(err, result){
            console.log(result);
            callback(err, result);
        });
    },
    
    updateUser:function(id, User, callback){
        db.query("update users set name=?,email=?,password=?,company=? where user_id=?",[User.name,User.email,User.password,User.company,id],function(err, result){
            console.log(result);
            callback(err, result);
        });
    }
}

module.exports = users;