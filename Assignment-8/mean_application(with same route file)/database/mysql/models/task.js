var db=require('../util/dbconnect'); 

var task = {
    addTask: function(Task, callback){
        db.query("INSERT INTO tasks(user_id, project, task, description, created_on, due_date, assigned_to) values(?,?,?,?,?,?,?)", [Task.userId, Task.project, Task.task, Task.description, Task.created_on, Task.due_date, Task.assigned_to], function(err,result){
            console.log(result);
            callback(err, result);
        });
    },
    
    getAllTasks: function(callback){
        db.query("Select * from tasks", function(err,result){
            console.log(result);
            callback(err, result);   
        });
    },
    
    getTaskById: function(id, callback){
        db.query("Select * from tasks where user_id=?",[id],function(err, result){
            console.log(result);
            callback(err, result);
        })
    },
    
    deleteTask: function(id, callback){
        db.query("delete from tasks where task_id=?",[id],function(err, result){
            console.log(result);
            callback(err, result);
        });
    },
    
    updateTask:function(Task, callback){
        db.query("update tasks set project=?,task=?,description=?,due_date=?,assigned_to=? where task_id=?",[Task.project,Task.task,Task.description,Task.due_date,Task.assigned_to,Task.task_id],function(err, result){
            console.log(result);
            callback(err, result);
        });
    }
}

module.exports = task;