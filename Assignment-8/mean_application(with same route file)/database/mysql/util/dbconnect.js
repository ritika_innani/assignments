var mysql = require('mysql');
var config = require('./config');

var connection = mysql.createConnection({
  host: config.dbConfig.host,
  user: config.dbConfig.user,
  password: config.dbConfig.password,
  database: config.dbConfig.database
});

connection.connect(function(err){
  if (err){
      console.log('cannot connect to database');
      throw err;
  } 
  console.log('Connected!');
});

module.exports = connection;