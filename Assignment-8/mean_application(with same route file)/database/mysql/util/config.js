module.exports = {
    dbConfig: {
      host: 'localhost',
      user: 'root',
      password: '1234',
      database: 'task_management_system'
    },
    
    port: 8000,
    
    cors: {
        origin: 'http://localhost:4200'
    }
}

