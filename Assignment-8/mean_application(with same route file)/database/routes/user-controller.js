//var User = require('../mysql/models/user');
var User = require('../mongodb/models/user');


module.exports = function(router){
    router.post('/user', function(req, res){
        if(!req.body.name){
            res.json({success:false, message: 'Must provide a name'});
        } else{
            if(!req.body.email){
                res.json({success:false, message: 'Must provide an Email'});
            } else{
                if(!req.body.password){
                    res.json({success:false, message: 'Must provide password'});
                } else{
                    if(!req.body.company){
                        res.json({success:false, message: 'Must provide Company Name'});
                    } else{
                         var user = {
                            "name": req.body.name,
                            "email": req.body.email,
                            "password": req.body.password,
                            "company": req.body.company
                         }
                         console.log(user);
                         User.addUser(user,function(err,result){
                            if(err){
                                res.json({success: false, message: 'could not save user', err})
                            }
                            else{
                                if(result){
                                    res.json({success: true, message: 'User Saved', result: result.insertId});
                                } else{
                                    res.json({success: true, message: 'User Registered Successfully!'});
                                }
                            }
                        });   
                    }
                }
            }
        }
    });
    
    router.get('/user', function(req,res){
        User.getAllUsers(function(err, result){
            if(err){
                res.json({success:false, message:'could not get users', err});
            } else{
                if(result){
                    res.json({success:true, message:'users found'});
                } else{
                    res.json({success:false, message:'no users found'});
                }
            }
        })
    });
    
    return router;
}

//var User = getDbObject("User");
//
//function getDbObject(type){
//    if(config == "mysql")
//        {
//            
//        }
//    var temp_User= require('../mysql/models/user');
//    return temp_User;
//}