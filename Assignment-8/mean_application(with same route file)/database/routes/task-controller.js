var Task = require('../mongodb/models/task');
//var Task = require('../mysql/models/task');


module.exports = function(router){
    router.post('/task', function(req, res){
        var today=new Date();
        if(!req.body.project){  
            res.json({success:false, message: 'Must provide a project name'});
        } else{
            if(!req.body.task){
                res.json({success:false, message: 'Must provide a task name'});
            } else{
                let task={
                    "user_id": req.body.user_id,
                    "project": req.body.project,
                    "task": req.body.task,
                    "description": req.body.description,
                    "created_on": today,
                    "due_date": req.body.due_date,
                    "assigned_to": req.body.assigned_to
                };
                console.log(task);
                Task.addTask(task,function(err,result){
                    if(err){
                        res.json({success: false, message: 'could not save task', err})
                    }
                    else{
                        res.json({success: true, message: 'Task Saved',result});
                    }
                });
              }
          }
    });
    
    router.get('/task/:id', function(req,res){
        if(req.params.id){
            Task.getTaskById(req.params.id, function(err,result){
                if(err){
                    res.json({success: false, message: err});
                } else{
                    if(!result){
                        res.json({success: false, message: 'No tasks found'});
                    } else{
                        res.json({success: true, tasks:result});
                    }
                }
            });
        } else{
            Task.getAllTasks(function(err,result){
                if(err){
                    res.json({success: false, message: err});
                } else{
                    if(!result){
                        res.json({success: false, message: 'No tasks found'});
                    } else{
                        res.json({success: true, tasks:result});
                    }
                }
            });
          }
    });
    
    router.delete('/task/:id', function(req,res){
        if(!req.params.id){
            res.json({success: false, message: 'No id provided'});
        } else{
            Task.deleteTask(req.params.id, function(err,count){
                if(err){
                    res.json({success: false, message: err});
                } else{
                    res.json({success: true, message: 'Task Deleted', count});
                }
            });  
        }    
    });
    
    router.put('/task/:id', function(req,res){
        let task={
            "task_id":req.params.id,
            "project": req.body.project,
            "task": req.body.task,
            "description": req.body.description,
            "due_date": req.body.due_date,
            "assigned_to": req.body.assigned_to
        };
        Task.updateTask(task, function(err, result){
            
        });
    });
    
    return router;
}


