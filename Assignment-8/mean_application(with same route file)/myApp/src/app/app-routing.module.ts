import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TaskComponent } from './components/task/task.component';
import { DeleteTaskComponent } from './components/task/delete-task/delete-task.component';
import { UpdateTaskComponent } from './components/task/update-task/update-task.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { TaskFilterComponent } from './components/task-filter/task-filter.component';


const routes: Routes=[
    {path: '', component: HomeComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'login', component: LoginComponent},
    {path: 'dashboard', component: TaskComponent},
    {path: 'mytasks', component: TaskComponent},
    {path: 'delete-task/:id', component: DeleteTaskComponent},
    {path: 'update-task/:id', component: UpdateTaskComponent},
    {path: 'filter-task/:task', component: TaskFilterComponent},
    { path: '**', component: HomeComponent }
];

@NgModule({
    declarations: [],
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]
})

export class AppRoutingModule{}