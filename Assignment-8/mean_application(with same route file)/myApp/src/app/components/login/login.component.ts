import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

import { AuthenticateService } from '../../services/authenticate.service';
import { UtilityService } from '../../services/utility.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    messageClass;
    message;

    constructor(private authenticateService: AuthenticateService,
    private router: Router,
    private utility: UtilityService) { }
    
    ngOnInit():void {
        this.utility.isLogged().then((result: boolean) => {
            if(result){
                this.router.navigate(['/dashboard']);
            }
        });
    }

    onSubmit(value: any){
        console.log(value);
        console.log(value.email);
        var self = this;
        // for authenticating the user
        this.authenticateService.login(value).subscribe(data => {
          if(!data.success) {
            this.messageClass = 'alert alert-danger';
            this.message = data.message; 
          } else{
            this.messageClass = 'alert alert-success'; 
            this.message = data.message; 
            
            if(typeof(Storage) != 'undefined'){
                sessionStorage.setItem('User', value.email);
            }
            self.router.navigate(['/dashboard']);
          }
        });
    }
}
