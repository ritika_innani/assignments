import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticateService } from '../../services/authenticate.service';
import { UtilityService } from '../../services/utility.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})

export class NavbarComponent implements OnInit {
    display = true;
    
    constructor(private authenticateService: AuthenticateService,
    private router: Router,
    private utility: UtilityService) { 
//        router.events.subscribe(function(url: any){
//            url = router.url;
//            console.log(url);     
//        });
    }

  ngOnInit():void {
        this.utility.isLogged().then((result: boolean) => {
            if(result){
                this.display = false;
            }
        });
    }
}
