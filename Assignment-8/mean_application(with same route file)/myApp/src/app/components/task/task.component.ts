import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ModalDirective,ModalModule } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { DeleteTaskComponent} from './delete-task/delete-task.component';
import { AuthenticateService } from '../../services/authenticate.service';
import { TaskService } from '../../services/task.service';
import { UtilityService } from '../../services/utility.service';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
})

export class TaskComponent implements OnInit {

    newTaskBlock = false;
    rightPanel = false;
    leftPanel = false;
    message;
    messageClass;
    allTasks;
    @ViewChild('deletePopup') deletePopup :DeleteTaskComponent;
    
    constructor(private authenticateService: AuthenticateService, 
    private taskService: TaskService, 
    private router: Router, 
    private utility: UtilityService,
    private viewContainerRef: ViewContainerRef) {}
    
    ngOnInit() {
        this.utility.isLogged().then((result: boolean) => {
            if(!result){
                this.router.navigate(['/login']);
            } else{
                this.getAllTasks();
            }
        });   
    }
    
    showTaskBlock(){
        this.newTaskBlock = true;
        this.rightPanel = true;
        this.leftPanel = true;
    }

    hideTaskBlock(){
        this.rightPanel = false;
        this.leftPanel = false;
    }
    
//this.email = route.snapshot.params[email];
    onSubmit(value: any) {
        console.log(value);
        var self = this;

    // Function from authentication service to register user
        this.taskService.newTask(value).subscribe((data) => {
            if (!data.success) {
                this.messageClass = 'alert alert-danger'; 
                this.message = data.message;
                alert(data.message);

            } else {
                this.messageClass = 'alert alert-success'; 
                this.message = data.message; 
                alert(data.message);
                this.rightPanel = false;
                this.leftPanel = false;
                self.router.navigate(['/dashboard']);
                this.getAllTasks();
                //window.location.reload();
            }
        });
    }
    
    getAllTasks(){
        this.taskService.getAllTasks().subscribe(data => {
            this.allTasks = data.tasks;
        });
    }
}
 