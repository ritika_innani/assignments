import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AuthenticateService } from '../../../services/authenticate.service';
import { TaskService } from '../../../services/task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-task',
  templateUrl: './delete-task.component.html',
  styleUrls: ['./delete-task.component.css'],
inputs: [`id`]
})
export class DeleteTaskComponent implements OnInit {
    currentUrl;
    public id;
    
    @ViewChild('deletePopup') public deletePopup:ModalDirective;
    
    constructor(private authenticateService: AuthenticateService,
    private taskService: TaskService,
    private router: Router ) { }
    
    ngOnInit() {
        
    }
        
    show(){
        this.deletePopup.show();
    }

    hide(){
        this.deletePopup.hide();
    }
    
    deleteTask(){
    this.taskService.deleteTask(this.id).subscribe(data => {
        var self = this;
        if (!data.success) {
                console.log(data.message);
                alert('cannot delete');
            } else {
                console.log(data.message);
                alert('deleted');
                this.hide();
                self.router.navigate(['/dashboard']); 
            }
    });
    }
    
    return(){
        var self = this;
        self.router.navigate(['/dashboard']);
    }
}
