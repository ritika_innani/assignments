import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ModalDirective,ModalModule } from 'ngx-bootstrap';
//import { FlashMessagesModule } from 'angular2-flash-messages';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TaskComponent } from './components/task/task.component';
import { SubnavbarComponent } from './components/subnavbar/subnavbar.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AuthenticateService } from './services/authenticate.service';
import { TaskService } from './services/task.service';
import { UtilityService } from './services/utility.service';
import { UpdateTaskComponent } from './/components/task/update-task/update-task.component';
import { DeleteTaskComponent } from './components/task/delete-task/delete-task.component';
import { TaskFilterComponent } from './components/task-filter/task-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TaskComponent,
    SubnavbarComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    SidebarComponent,
    UpdateTaskComponent,
    DeleteTaskComponent,
    TaskFilterComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpModule, ReactiveFormsModule, FormsModule, ModalModule.forRoot()
  ],
  providers: [
    AuthenticateService, 
    TaskService, 
    UtilityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
