import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticateService {
    domain = "http://localhost:8000";
    
    constructor( private http: Http ) { 
    }
    
    // function to register new user
    registerUser(user) {
        return this.http.post(this.domain + '/api/register', user).map(res => res.json());
    }
    
    // Function to login user
    login(user) {
        return this.http.post(this.domain + '/api/authenticate', user).map(res => res.json());
    }

}
