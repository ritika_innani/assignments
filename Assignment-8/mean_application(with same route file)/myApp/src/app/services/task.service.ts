import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TaskService {
    domain = "http://localhost:8000";

  constructor(private http: Http) { }
    
    newTask(task) {
        return this.http.post(this.domain + '/api/task', task).map(res => res.json());
    }
    
    getAllTasks(){
        return this.http.get(this.domain + '/api/task').map(res => res.json());
    }
    
    deleteTask(id){
        return this.http.delete(this.domain + '/api/task/' + id).map(res => res.json());
    }

}
